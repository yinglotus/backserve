package com.example.showcar.controller;

import com.example.showcar.bean.Forum;
import com.example.showcar.service.ForumService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.text.Normalizer;
import java.util.List;

/**
 * (Forum)表控制层
 *
 * @author makejava
 * @since 2020-08-21 18:30:27
 */
@RestController
@RequestMapping("api/forum")
public class ForumController {
    /**
     * 服务对象
     */
    @Autowired
    private ForumService forumService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @RequestMapping("selectOne")
    public List<Forum> selectOne(Forum forum) {
        List<Forum> forums = forumService.queryAll(forum);
        return forums;
    }
    @RequestMapping("test1")
    public List<Forum> test1(Forum forum){
        List<Forum> forums = forumService.queryAll(forum);
        return forums;
    }

}