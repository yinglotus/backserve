package com.example.showcar.controller;

import com.example.showcar.bean.Piedatas;
import com.example.showcar.service.PiedatasService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * (Piedatas)表控制层
 *
 * @author makejava
 * @since 2020-11-13 11:02:17
 */
@RestController
@RequestMapping("/api/piedatas")
public class PiedatasController {
    /**
     * 服务对象
     */
    @Resource
    private PiedatasService piedatasService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne")
    public Piedatas selectOne(Integer id) {
        return this.piedatasService.queryById(id);
    }

    @RequestMapping("/queryAll")
    public List<Piedatas> queryAll(Piedatas piedatas){
        List<Piedatas> piedatas1 = this.piedatasService.queryAll(piedatas);
        return piedatas1;
    }

    @RequestMapping("/queryAllcategoryByBrand")
    public List<Piedatas> queryAllcategoryByBrand(Piedatas piedatas){
        piedatas.setBrand(piedatas.getBrand().substring(0,2) + "%");
        List<Piedatas> piedatas1 = this.piedatasService.queryAllcategoryByBrand(piedatas);
        return piedatas1;
    }

}