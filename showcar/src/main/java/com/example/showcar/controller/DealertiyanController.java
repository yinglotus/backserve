package com.example.showcar.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/dealertiyan")
public class DealertiyanController {

    @RequestMapping("/getdatas")
    public List<Float> test(String brand){
        List<Float> result = new ArrayList<>(8);
        if(brand.equals("SVW")){
            result.add(new Float("3.62"));
            result.add(new Float("3.63"));
            result.add(new Float("3.63"));
            result.add(new Float("3.63"));
            result.add(new Float("3.61"));
            result.add(new Float("3.59"));
            result.add(new Float("3.59"));
            result.add(new Float("3.59"));
        }
        if(brand.equals("Skoda")){
            result.add(new Float("3.58"));
            result.add(new Float("3.55"));
            result.add(new Float("3.53"));
            result.add(new Float("3.52"));
            result.add(new Float("3.52"));
            result.add(new Float("3.53"));
            result.add(new Float("3.62"));
            result.add(new Float("3.61"));
        }
        if(brand.equals("FAW-VW")){
            result.add(new Float("3.56"));
            result.add(new Float("3.54"));
            result.add(new Float("3.52"));
            result.add(new Float("3.52"));
            result.add(new Float("3.48"));
            result.add(new Float("3.44"));
            result.add(new Float("3.46"));
            result.add(new Float("3.44"));
        }
        return result;
    }

}
