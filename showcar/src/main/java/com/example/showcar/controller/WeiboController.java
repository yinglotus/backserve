package com.example.showcar.controller;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestController
@RequestMapping("/api/weibo")
public class WeiboController {

    @RequestMapping("/getUserInfo")
    public List<String> getuserInfo(@RequestParam("brand")String brand) {
//        try {
//            Thread.currentThread().sleep(((int) 500 + (int) (Math.random() * (1200 - 500))));
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
        String urlStr = null;
        List<String> result = new ArrayList<>();
        if (brand.equals("奔驰")){
            urlStr = "https://weibo.com/mymb?is_all=1";
        }else if(brand.equals("宝马")){
            urlStr = "https://weibo.com/bmwchina?is_all=1";
        }else if(brand.equals("奥迪")){
            urlStr = "https://weibo.com/iaudi?is_all=1";
        }else if(brand.equals("蔚来")){
            urlStr = "https://weibo.com/nextevofficial?is_all=1";
        }else if(brand.equals("特斯拉")){
            urlStr = "https://weibo.com/teslaofficial?is_all=1";
        }
        try {
            Document doc = Jsoup.connect(urlStr)
                    .userAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36")
                    .cookie("Cookie", "SINAGLOBAL=6012235092197.995.1606094429873; _s_tentry=login.sina.com.cn; Apache=6854031100654.33.1611845749347; ULV=1611845749353:4:3:1:6854031100654.33.1611845749347:1610351308129; login_sid_t=23e63dc8ddc301252c1bbdb8bd65d5bd; cross_origin_proto=SSL; WBtopGlobal_register_version=2021012912; appkey=; TC-V-WEIBO-G0=b09171a17b2b5a470c42e2f713edace0; XSRF-TOKEN=gh8TtfHZwVrSnx4kfBCXC7AZ; SSOLoginState=1612336428; SCF=AkGXPGarY40LZTJY_IR-M5lgi_sg5O_T9fF0IlbExsgRoGQ9sNNSoOb6vJadF2LDe08B9XNzjWjS1aXMtT1fCuw.; ALF=1643979744; UOR=,,www.baidu.com; wb_timefeed_7557801476=1; SUB=_2AkMXQe6Yf8NxqwJRmP4VymzqaIh3ygrEieKhHR9DJRMxHRl-yT9kqmk_tRB6PMHAd1aAZnrDmYGpkiW3iY5EfnaxIY5A; SUBP=0033WrSXqPxfM72-Ws9jqgMF55529P9D9WFo2iEpsQTpZI4QgX6DeZ_I; wb_view_log_7557801476=1440*9002; webim_unReadCount={\"time\":1612686830369,\"dm_pub_total\":0,\"chat_group_client\":0,\"chat_group_notice\":0,\"allcountNum\":0,\"msgbox\":0}")
                    .get();//获取html
            String regex = "<strong class=\\\\\"W_f14\\\\\">(.*?)<\\\\/strong>";
            Pattern pattern = Pattern.compile(regex);
            Matcher matcher = pattern.matcher(doc.html());
            while (matcher.find()) {
                result.add(matcher.group(1));
            }
            if(result.size()==0){
                String regex1 = "<strong class=\\\\\"W_f16\\\\\">(.*?)<\\\\/strong>";
                Pattern pattern1 = Pattern.compile(regex1);
                Matcher matcher1 = pattern1.matcher(doc.html());
                while (matcher1.find()) {
                    result.add(matcher1.group(1));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        //增加系统时间
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
        result.add(df.format(new Date()));
        return result;
    }

    @RequestMapping("/getHuatiInfo")
    public List<String> getHuatiInfo(@RequestParam("brand")String brand){
//        try {
//            Thread.currentThread().sleep(((int) 500 + (int) (Math.random() * (1200 - 500))));
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
        String urlStr = null;
        List<String> result = new ArrayList<>();
        if (brand.equals("奔驰")){
            urlStr = "https://s.weibo.com/weibo?q=%23%E5%A5%94%E9%A9%B0%23&Refer=index&display=0&retcode=6102";
        }else if(brand.equals("宝马")){
            urlStr = "https://s.weibo.com/weibo?q=%23%E5%AE%9D%E9%A9%AC%23&Refer=index&display=0&retcode=6102";
        }else if(brand.equals("奥迪")){
            urlStr = "https://s.weibo.com/weibo?q=%23%E5%A5%A5%E8%BF%AA%23&Refer=index&display=0&retcode=6102";
        }else if(brand.equals("蔚来")){
            urlStr = "https://s.weibo.com/weibo?q=%23%E8%94%9A%E6%9D%A5%23&Refer=index&display=0&retcode=6102";
        }else if(brand.equals("特斯拉")){
            urlStr = "https://s.weibo.com/weibo?q=%23%E7%89%B9%E6%96%AF%E6%8B%89%23&Refer=index&display=0&retcode=6102";
        }
        try {
            Document doc = Jsoup.connect(urlStr)
                    .userAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Safari/537.36")
                    .cookie("Cookie","SINAGLOBAL=6012235092197.995.1606094429873; SCF=AkGXPGarY40LZTJY_IR-M5lgi_sg5O_T9fF0IlbExsgRoGQ9sNNSoOb6vJadF2LDe08B9XNzjWjS1aXMtT1fCuw.; login_sid_t=c47dfb5a939985f0753a53639b35213b; cross_origin_proto=SSL; _s_tentry=www.baidu.com; UOR=,,www.baidu.com; Apache=2922397351912.8486.1612790693546; ULV=1612790693551:5:1:1:2922397351912.8486.1612790693546:1611845749353; SUB=_2A25NJUv_DeRhGeFL7lUZ8C_IzDqIHXVuUzo3rDV8PUNbmtAKLXHRkW9NffOZwX2cupqOS6Ke8NSrhfovJlMJYT-J; SUBP=0033WrSXqPxfM725Ws9jqgMF55529P9D9WhdNLjvaxv0pSog3ghgYKRO5JpX5KzhUgL.FoMfSKMReh2XS0q2dJLoIE__dNiVIg4ri--NiKy8iKLFi--fiKy2i-2pi--fi-zNi-zXi--fiKy2i-2p; ALF=1644326703; SSOLoginState=1612790703; wvr=6; webim_unReadCount={\"time\":1612791132480,\"dm_pub_total\":0,\"chat_group_client\":0,\"chat_group_notice\":0,\"allcountNum\":1,\"msgbox\":0}")
                    .get();//获取html
            String regex = "<span>阅读(.*?)</span>";
            Pattern pattern = Pattern.compile(regex);
            Matcher matcher = pattern.matcher(doc.html());
            matcher.find();
            result.add(matcher.group(1));

            String regex1 = "<span>讨论(.*?)</span>";
            Pattern pattern1 = Pattern.compile(regex1);
            Matcher matcher1 = pattern1.matcher(doc.html());
            matcher1.find();
            result.add(matcher1.group(1));

        } catch (IOException e) {
            e.printStackTrace();
        }
        //增加系统时间
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
        result.add(df.format(new Date()));
        return result;
    }
}
