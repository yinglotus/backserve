package com.example.showcar.controller;

import com.example.showcar.bean.Discount;
import com.example.showcar.bean.RetunDataOfByBrand;
import com.example.showcar.service.DiscountService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * (Discount)表控制层
 *
 * @author makejava
 * @since 2020-10-31 20:23:58
 */
@RestController
@RequestMapping("/api/discount")
public class DiscountController {
    /**
     * 服务对象
     */
    @Resource
    private DiscountService discountService;

    /**
     * 通过主键查询单条数据
     *
     * @param
     * @return 单条数据
     *
     * /
    /**
     * 按照品牌年月查询
     */
    @RequestMapping("/selectByBrandYearMonth")
    public List<RetunDataOfByBrand> selectByBrandYearMonth(Discount discount){
        discount.setBrand(discount.getBrand()+"%");
        List<RetunDataOfByBrand> retunDataOfByBrands = new ArrayList<RetunDataOfByBrand>(12);
        LocalDate today = LocalDate.now();
        /**
         * 获取到目前为止十二个月的信息
         */
        for (int i = 0; i < 12; i++) {
            LocalDate today1 = today.minusMonths(i);
            RetunDataOfByBrand data = new RetunDataOfByBrand();
            data.setBuyyear(today1.toString().substring(0,4));
            data.setBuymonth(today1.toString().substring(5,7));
            data.setEnglishbuymonth(today1.getMonth().toString());

            retunDataOfByBrands.add(data);
        }
        /***
         * 开始查询
         */
        for (int i = 0; i < 12; i++) {
            discount.setBuyyear(retunDataOfByBrands.get(i).getBuyyear());
            discount.setBuymonth(retunDataOfByBrands.get(i).getBuymonth());
            List<Discount> discounts = this.discountService.selectByBrandYearMonth(discount);
            if(discounts.size()==0) {
                retunDataOfByBrands.get(i).setDiscount("0.00");
            }else {
                float sum = 0;
                for (int j = 0; j < discounts.size(); j++) {
                    sum = sum + Float.parseFloat(discounts.get(j).getDiscount());
                }
                DecimalFormat decimalFormat=new DecimalFormat("0.00");
                String ave = decimalFormat.format(sum/discounts.size());
                retunDataOfByBrands.get(i).setDiscount(ave);
                retunDataOfByBrands.get(i).setEnglishBrand(discounts.get(0).getEnglishbrand());
            }
        }
        return retunDataOfByBrands;
    }


    /**
     * 按照车系查找数据函数
     */
    List<RetunDataOfByBrand> search(Discount discount){
        List<RetunDataOfByBrand> retunDataOfByBrands = new ArrayList<RetunDataOfByBrand>();
        LocalDate today = LocalDate.now();
        /**
         * 获取到目前为止12个月的信息
         */
        for (int i = 0; i < 12; i++) {
            LocalDate today1 = today.minusMonths(i);
            RetunDataOfByBrand data = new RetunDataOfByBrand();
            data.setBuyyear(today1.toString().substring(0,4));
            data.setBuymonth(today1.toString().substring(5,7));
            data.setEnglishbuymonth(today1.getMonth().toString());
            retunDataOfByBrands.add(data);
        }
        /**
         * 开始查询
         */
        for (int i = 0; i < 12; i++) {
            discount.setBuyyear(retunDataOfByBrands.get(i).getBuyyear());
            discount.setBuymonth(retunDataOfByBrands.get(i).getBuymonth());
            List<Discount> discounts = this.discountService.selectByCarseries(discount);
            if(discounts.size()==0) {
                retunDataOfByBrands.get(i).setDiscount("0.00");
            }else {
                float sum = 0;
                for (int j = 0; j < discounts.size(); j++) {

                    sum = sum + Float.parseFloat(discounts.get(j).getDiscount());
                }
                DecimalFormat decimalFormat=new DecimalFormat("0.00");
                String ave = decimalFormat.format(sum/discounts.size());
                retunDataOfByBrands.get(i).setDiscount(ave);
            }
            retunDataOfByBrands.get(i).setCarseries(discount.getCarseries());
        }
        return retunDataOfByBrands;
    }

    /**
     * 按照车系查询
     */
    @RequestMapping("/selectByCarseries")
    public ArrayList selectByCarseries(Discount discount){
        ArrayList returnlist = new ArrayList();
        //奔驰
        if(discount.getBrand().equals("Mercedes-Benz")) {
            //查找A-Class
            discount.setCarseries("A-Class");
            returnlist.add(search(discount));
            //查找C-Class
            discount.setCarseries("C-Class");
            returnlist.add(search(discount));
            //查找E-Class
            discount.setCarseries("E-Class");
            returnlist.add(search(discount));
            //查找S-Class
            discount.setCarseries("S-Class");
            returnlist.add(search(discount));
            //查找GLC
            discount.setCarseries("GLC");
            returnlist.add(search(discount));
            //查找GLE
            discount.setCarseries("GLE");
            returnlist.add(search(discount));
            //查找GLS
            discount.setCarseries("GLS");
            returnlist.add(search(discount));
        }
        //宝马
        if(discount.getBrand().equals("BMW")) {
            //宝马1Series
            discount.setCarseries("1 Series");
            returnlist.add(search(discount));
            //宝马3Series
            discount.setCarseries("3 Series");
            returnlist.add(search(discount));
            //宝马5Series
            discount.setCarseries("5 Series");
            returnlist.add(search(discount));
            //宝马7 series
            discount.setCarseries("7 Series");
            returnlist.add(search(discount));
            //宝马X1
            discount.setCarseries("X1");
            returnlist.add(search(discount));
            //宝马X3
            discount.setCarseries("X3");
            returnlist.add(search(discount));
            //宝马X5
            discount.setCarseries("X5");
            returnlist.add(search(discount));
            //宝马X7
            discount.setCarseries("X7");
            returnlist.add(search(discount));

        }
        //奥迪
        if(discount.getBrand().equals("Audi")) {
            //A4L
            discount.setCarseries("A4L");
            returnlist.add(search(discount));
            //A6L
            discount.setCarseries("A6L");
            returnlist.add(search(discount));
            //Q5L
            discount.setCarseries("Q5L");
            returnlist.add(search(discount));
            //Q7
            discount.setCarseries("Q7");
            returnlist.add(search(discount));
            //A3
            discount.setCarseries("A3");
            returnlist.add(search(discount));
            //A8
            discount.setCarseries("A8");
            returnlist.add(search(discount));
            //Q3
            discount.setCarseries("Q3");
            returnlist.add(search(discount));
        }
        //凯迪拉克
        if(discount.getBrand().equals("Cadillac")) {
            //CT4
            discount.setCarseries("CT4");
            returnlist.add(search(discount));
            //CT5
            discount.setCarseries("CT5");
            returnlist.add(search(discount));
            //CT6
            discount.setCarseries("CT6");
            returnlist.add(search(discount));
            //XT4
            discount.setCarseries("XT4");
            returnlist.add(search(discount));
            //XT5
            discount.setCarseries("XT5");
            returnlist.add(search(discount));
            //XT6
            discount.setCarseries("XT6");
            returnlist.add(search(discount));
        }
        //路虎
        if(discount.getBrand().equals("Land Rover")) {
            //路虎揽胜
            discount.setCarseries("Range Rover");
            returnlist.add(search(discount));
            //路虎揽胜运动版
            discount.setCarseries("Range Rover Sport");
            returnlist.add(search(discount));
            //路虎揽胜星脉
            discount.setCarseries("Range Rover Velar");
            returnlist.add(search(discount));
            //路虎揽胜极光
            discount.setCarseries("Evoque");
            returnlist.add(search(discount));
        }
        //上汽大众
        if(discount.getBrand().equals("SAIC Volkswagen")) {
            //polo
            discount.setCarseries("Polo");
            returnlist.add(search(discount));
            //桑塔拉
            discount.setCarseries("Santana");
            returnlist.add(search(discount));
            //
            discount.setCarseries("Lavida");
            returnlist.add(search(discount));
            discount.setCarseries("Lamando");
            returnlist.add(search(discount));
            discount.setCarseries("Passat");
            returnlist.add(search(discount));
            discount.setCarseries("Phideon");
            returnlist.add(search(discount));
            discount.setCarseries("T-Cross");
            returnlist.add(search(discount));
            discount.setCarseries("Tharu");
            returnlist.add(search(discount));
            discount.setCarseries("Tiguan L");
            returnlist.add(search(discount));
            discount.setCarseries("Teramont");
            returnlist.add(search(discount));
            discount.setCarseries("Touran");
            returnlist.add(search(discount));
            discount.setCarseries("Viloran");
            returnlist.add(search(discount));

        }
        //一汽大众
        if(discount.getBrand().equals("FAW-Volkswagen")) {
            discount.setCarseries("Bora");
            returnlist.add(search(discount));
            discount.setCarseries("Magotan");
            returnlist.add(search(discount));
            discount.setCarseries("Sagitar");
            returnlist.add(search(discount));
            discount.setCarseries("Tayron");
            returnlist.add(search(discount));
            discount.setCarseries("Golf");
            returnlist.add(search(discount));
            discount.setCarseries("T-Roc");
            returnlist.add(search(discount));
            discount.setCarseries("CC");
            returnlist.add(search(discount));
            discount.setCarseries("Tacqua");
            returnlist.add(search(discount));
            discount.setCarseries("C-Trek");
            returnlist.add(search(discount));
        }
        //蔚来
        if(discount.getBrand().equals("NIO")) {
            discount.setCarseries("ES6");
            returnlist.add(search(discount));
            discount.setCarseries("ES8");
            returnlist.add(search(discount));
            discount.setCarseries("EC6");
            returnlist.add(search(discount));
        }
        //威马
        if(discount.getBrand().equals("Weltmeister")) {
            discount.setCarseries("EX5");
            returnlist.add(search(discount));
            discount.setCarseries("EX6");
            returnlist.add(search(discount));
        }
        //小鹏
        if(discount.getBrand().equals("Xpeng")) {
            discount.setCarseries("P7");
            returnlist.add(search(discount));
            discount.setCarseries("G3");
            returnlist.add(search(discount));
        }
        //理想
        if(discount.getBrand().equals("Li Auto")) {
            discount.setCarseries("ONE");
            returnlist.add(search(discount));
        }
        //byd
        if(discount.getBrand().equals("BYD")) {
            discount.setCarseries("Qin EV");
            returnlist.add(search(discount));
            discount.setCarseries("Qin Pro EV");
            returnlist.add(search(discount));
            discount.setCarseries("Yuan EV");
            returnlist.add(search(discount));
            discount.setCarseries("Song Pro EV");
            returnlist.add(search(discount));
            discount.setCarseries("Tang EV");
            returnlist.add(search(discount));
            discount.setCarseries("Song Max EV");
            returnlist.add(search(discount));
            discount.setCarseries("Han EV");
            returnlist.add(search(discount));
        }
        return returnlist;
    }
}

















