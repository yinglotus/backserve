package com.example.showcar.controller;

import com.example.showcar.bean.User;
import com.example.showcar.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * (User)表控制层
 *
 * @author makejava
 * @since 2020-07-30 17:12:40
 */
@RestController
@RequestMapping("api/user")
public class UserController {
    /**
     * 服务对象
     */
    @Autowired
    private UserService userService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne")
    public User selectOne(Integer id) {
        return this.userService.queryById(id);
    }

    /**
     * 登陆查询
     */
    @RequestMapping("userLogin")
    public Boolean login(User user, HttpSession session, HttpServletResponse response){
        User principal = userService.login(user);
        if(principal != null){
            session.setAttribute("user",principal);
            return true;
        }
        response.setStatus(403);
        return false;
    }

    @RequestMapping("test1")
        public List<User> test(User user){
            List<User> users = this.userService.queryAll(user);
            return users;
    }

}