package com.example.showcar.controller;

import com.example.showcar.bean.Koubei;
import com.example.showcar.service.KoubeiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * (Koubei)表控制层
 *
 * @author makejava
 * @since 2020-08-08 16:01:45
 */
@RestController
@RequestMapping("api/koubei")
public class KoubeiController {
    /**
     * 服务对象
     */
    @Autowired
    private KoubeiService koubeiService;

    /**
     * 通过主键查询单条数据
     *
     * @param date 主键
     * @return 单条数据
     */
    @RequestMapping("selectOne")
    public Koubei selectOne(Koubei koubei,String date) {
        if(!date.equals("")){
            koubei.setCreatedate(java.sql.Date.valueOf(date));
        }
        List<Koubei> koubeis = koubeiService.queryBycarseries(koubei);
        Koubei result = new Koubei();
        DecimalFormat df   = new DecimalFormat("######0.00");
        result.setExterior(0.0);
        result.setInterior(0.0);
        result.setCarspace(0.0);
        result.setOptionsortrim(0.0);
        result.setPower(0.0);
        result.setManeuverability(0.0);
        result.setFuelconsumption(0.0);
        result.setComfort(0.0);

        for (int i=0;i<koubeis.size();i++){
            result.setExterior(result.getExterior()+koubeis.get(i).getExterior());
            result.setInterior(result.getInterior()+koubeis.get(i).getInterior());
            result.setCarspace(result.getCarspace()+koubeis.get(i).getCarspace());
            result.setOptionsortrim(result.getOptionsortrim()+koubeis.get(i).getOptionsortrim());
            result.setPower(result.getPower()+koubeis.get(i).getPower());
            result.setManeuverability(result.getManeuverability()+koubeis.get(i).getManeuverability());
            result.setFuelconsumption(result.getFuelconsumption()+koubeis.get(i).getFuelconsumption());
            result.setComfort(result.getComfort()+koubeis.get(i).getComfort());
        }
        result.setId(1);
        result.setBrand(koubeis.get(0).getBrand());
        result.setCarseries(koubeis.get(0).getCarseries());
        result.setExterior(Double.valueOf(df.format(result.getExterior()/koubeis.size())));
        result.setInterior(Double.valueOf(df.format(result.getInterior()/koubeis.size())));
        result.setCarspace(Double.valueOf(df.format(result.getCarspace()/koubeis.size())));
        result.setOptionsortrim(Double.valueOf(df.format(result.getOptionsortrim()/koubeis.size())));
        result.setPower(Double.valueOf(df.format(result.getPower()/koubeis.size())));
        result.setManeuverability(Double.valueOf(df.format(result.getManeuverability()/koubeis.size())));
        result.setFuelconsumption(Double.valueOf(df.format(result.getFuelconsumption()/koubeis.size())));
        result.setComfort(Double.valueOf(df.format(result.getComfort()/koubeis.size())));


        //       选出strength和weaknesses
        result.setStrengths("Exterior");
        Double strengthvalue = result.getExterior();
        if(strengthvalue<result.getInterior()){
            result.setStrengths("Interior");
            strengthvalue = result.getInterior();
        }
        if(strengthvalue<result.getCarspace()){
            result.setStrengths("Carspace");
            strengthvalue = result.getCarspace();
        }
        if(strengthvalue<result.getOptionsortrim()){
            result.setStrengths("Configuration/trim");
            strengthvalue = result.getOptionsortrim();
        }
        if(strengthvalue<result.getPower()){
            result.setStrengths("Power");
            strengthvalue = result.getPower();
        }
        if(strengthvalue<result.getManeuverability()){
            result.setStrengths("Maneuverability");
            strengthvalue = result.getManeuverability();
        }
        if(strengthvalue<result.getFuelconsumption()){
            result.setStrengths("Fuel consumption");
            strengthvalue = result.getFuelconsumption();
        }
        if(strengthvalue<result.getComfort()){
            result.setStrengths("Comfort");
            strengthvalue = result.getComfort();
        }

//        选出弱项
        result.setWeaknesses("Exterior");
        Double weaknesses = result.getExterior();
        if(weaknesses>result.getInterior()){
            result.setWeaknesses("Interior");
            weaknesses = result.getInterior();
        }
        if(weaknesses>result.getCarspace()){
            result.setWeaknesses("Carspace");
            weaknesses = result.getCarspace();
        }
        if(weaknesses>result.getOptionsortrim()){
            result.setWeaknesses("Configuration/trim");
            weaknesses = result.getOptionsortrim();
        }
        if(weaknesses>result.getPower()){
            result.setWeaknesses("Power");
            weaknesses = result.getPower();
        }
        if(weaknesses>result.getManeuverability()){
            result.setWeaknesses("Maneuverability");
            weaknesses = result.getManeuverability();
        }
        if(weaknesses>result.getFuelconsumption()){
            result.setWeaknesses("Fuel consumption");
            weaknesses = result.getFuelconsumption();
        }
        if(weaknesses>result.getComfort()){
            result.setWeaknesses("Comfort");
            weaknesses = result.getComfort();
        }
        result.setDatanum(koubeis.size());
        return result;
    }

    @RequestMapping("queryAll")
    public List<Koubei> queryAll(Koubei koubei){
        koubei.setBrand("%" + koubei.getBrand() + "%");
        List<Koubei> koubeis = koubeiService.queryAll(koubei);
        return koubeis;
    }
    @RequestMapping("queryAllNumByDate")
    public Integer queryAllNumByDate(String date){
        Integer sum = 0;
        Koubei koubei = new Koubei();
        koubei.setBrand(date);
        List<Koubei> koubeis = koubeiService.queryAllNumByDate(koubei);
        sum = koubeis.size();
        return sum;
    }

    @RequestMapping("autoSelectByBrand")
    public List<Koubei> autoSelectByBrand(Koubei koubei){
        List<Koubei> koubeis = new ArrayList<Koubei>(2);
        Koubei koubei1 = new Koubei();
        Koubei koubei2 = new Koubei();
        Koubei koubei3 = new Koubei();
        koubei1.setBrand("奔驰");
        koubei2.setBrand("宝马");
        koubei3.setBrand("奥迪");
        if(koubei.getBrand().equals("奔驰")){
            koubeis.add(koubei2);
            koubeis.add(koubei3);
        }
        if(koubei.getBrand().equals("宝马")){
            koubeis.add(koubei1);
            koubeis.add(koubei3);
        }
        if(koubei.getBrand().equals("奥迪")) {
            koubeis.add(koubei1);
            koubeis.add(koubei2);
        }
        return koubeis;
    }

    @RequestMapping("autoSelectByModel")
    public List<Koubei> autoSelectByModel(Koubei koubei){
        List<Koubei> koubeis = new ArrayList<Koubei>(2);
        Koubei koubei1 = new Koubei();
        Koubei koubei2 = new Koubei();
        Koubei koubei3 = new Koubei();
        if(koubei.getCarseries().equals("宝马1系") || koubei.getCarseries().equals("奔驰A级") || koubei.getCarseries().equals("奥迪A3")){
            koubei1.setCarseries("1 Series");
            koubei1.setBrand("宝马1系");
            koubeis.add(0,koubei1);

            koubei2.setCarseries("A-Class");
            koubei2.setBrand("奔驰A级");
            koubeis.add(1,koubei2);

            koubei3.setCarseries("A3");
            koubei3.setBrand("奥迪A3");
            koubeis.add(2,koubei3);
        }
        if(koubei.getCarseries().equals("奥迪A4L") || koubei.getCarseries().equals("宝马3系") || koubei.getCarseries().equals("奔驰C级")){
            koubei1.setCarseries("A4L");
            koubei1.setBrand("奥迪A4L");
            koubeis.add(0,koubei1);

            koubei2.setCarseries("3 Series");
            koubei2.setBrand("宝马3系");
            koubeis.add(1,koubei2);

            koubei3.setCarseries("C-Class");
            koubei3.setBrand("奔驰C级");
            koubeis.add(2,koubei3);
        }
        if(koubei.getCarseries().equals("奥迪A5") || koubei.getCarseries().equals("宝马4系")){
            koubei1.setCarseries("A5");
            koubei1.setBrand("奥迪A5");
            koubeis.add(0,koubei1);

            koubei2.setCarseries("4 Series");
            koubei2.setBrand("宝马4系");
            koubeis.add(1,koubei2);
        }
        if(koubei.getCarseries().equals("奥迪A6L") || koubei.getCarseries().equals("宝马5系") || koubei.getCarseries().equals("奔驰E级z")){
            koubei1.setCarseries("A6L");
            koubei1.setBrand("奥迪A6L");
            koubeis.add(0,koubei1);

            koubei2.setCarseries("5 Series");
            koubei2.setBrand("宝马5系");
            koubeis.add(1,koubei2);

            koubei3.setCarseries("E-Class");
            koubei3.setBrand("奔驰E级");
            koubeis.add(2,koubei3);
        }
        if(koubei.getCarseries().equals("奥迪A7") || koubei.getCarseries().equals("宝马6系GT")){
            koubei1.setCarseries("A7");
            koubei1.setBrand("奥迪A7");
            koubeis.add(0,koubei1);

            koubei2.setCarseries("6 Series");
            koubei2.setBrand("宝马6系GT");
            koubeis.add(1,koubei2);
        }
        if(koubei.getCarseries().equals("奥迪A8") || koubei.getCarseries().equals("宝马7系") || koubei.getCarseries().equals("奔驰S级")){
            koubei1.setCarseries("A8");
            koubei1.setBrand("奥迪A8");
            koubeis.add(0,koubei1);

            koubei2.setCarseries("7 Series");
            koubei2.setBrand("宝马7系");
            koubeis.add(1,koubei2);

            koubei3.setCarseries("S-Class");
            koubei3.setBrand("奔驰S级");
            koubeis.add(2,koubei3);
        }
        if(koubei.getCarseries().equals("宝马8系") || koubei.getCarseries().equals("奔驰GLS")){
            koubei1.setCarseries("8 Series");
            koubei1.setBrand("宝马8系");
            koubeis.add(0,koubei1);

            koubei2.setCarseries("GLS");
            koubei2.setBrand("奔驰GLS");
            koubeis.add(1,koubei2);
        }
        if(koubei.getCarseries().equals("宝马X1") || koubei.getCarseries().equals("奔驰GLA")){
            koubei1.setCarseries("X1");
            koubei1.setBrand("宝马X1");
            koubeis.add(0,koubei1);

            koubei2.setCarseries("GLA");
            koubei2.setBrand("奔驰GLA");
            koubeis.add(1,koubei2);
        }
        if(koubei.getCarseries().equals("奥迪Q5L") || koubei.getCarseries().equals("宝马X3") || koubei.getCarseries().equals("奔驰GLC")){
            koubei1.setCarseries("Q5L");
            koubei1.setBrand("奥迪Q5L");
            koubeis.add(0,koubei1);

            koubei2.setCarseries("X3");
            koubei2.setBrand("宝马X3");
            koubeis.add(1,koubei2);

            koubei3.setCarseries("GLC");
            koubei3.setBrand("奔驰GLC");
            koubeis.add(2,koubei3);
        }
        if(koubei.getCarseries().equals("宝马X4") || koubei.getCarseries().equals("奔驰GLB")){
            koubei1.setCarseries("X4");
            koubei1.setBrand("宝马X4");
            koubeis.add(0,koubei1);

            koubei2.setCarseries("GLB");
            koubei2.setBrand("奔驰GLB");
            koubeis.add(1,koubei2);
        }
        if(koubei.getCarseries().equals("奥迪Q7") || koubei.getCarseries().equals("宝马X5") || koubei.getCarseries().equals("奔驰GLE")){
            koubei1.setCarseries("Q7");
            koubei1.setBrand("奥迪Q7");
            koubeis.add(0,koubei1);

            koubei2.setCarseries("X5");
            koubei2.setBrand("宝马X5");
            koubeis.add(1,koubei2);

            koubei3.setCarseries("GLE");
            koubei3.setBrand("奔驰GLE");
            koubeis.add(2,koubei3);
        }
        if(koubei.getCarseries().equals("奥迪Q8") || koubei.getCarseries().equals("宝马X7") || koubei.getCarseries().equals("奔驰GLS")){
            koubei1.setCarseries("Q8");
            koubei1.setBrand("奥迪Q8");
            koubeis.add(0,koubei1);

            koubei2.setCarseries("X7");
            koubei2.setBrand("宝马X7");
            koubeis.add(1,koubei2);

            koubei3.setCarseries("GLS");
            koubei3.setBrand("奔驰GLS");
            koubeis.add(2,koubei3);
        }
        if(koubei.getCarseries().equals("奥迪TT") || koubei.getCarseries().equals("宝马Z4") || koubei.getCarseries().equals("奔驰SLC")){
            koubei1.setCarseries("TT");
            koubei1.setBrand("奥迪TT");
            koubeis.add(0,koubei1);

            koubei2.setCarseries("Z4");
            koubei2.setBrand("宝马Z4");
            koubeis.add(1,koubei2);

            koubei3.setCarseries("SLC");
            koubei3.setBrand("奔驰SLC");
            koubeis.add(2,koubei3);
        }
        if(koubei.getCarseries().equals("奥迪e-tron(进口)") || koubei.getCarseries().equals("奔驰EQC")){
            koubei1.setCarseries("e-tron(Import)");
            koubei1.setBrand("奥迪e-tron(进口)");
            koubeis.add(0,koubei1);

            koubei2.setCarseries("EQC");
            koubei2.setBrand("奔驰EQC");
            koubeis.add(1,koubei2);
        }

        return koubeis;
    }

    @RequestMapping("selectAllByBrand")
    public Koubei selectByBrand(Koubei koubei,String date){
        Koubei result = new Koubei();

        koubei.setBrand("%" + koubei.getBrand() + "%");
        if(!date.equals("")) {
            koubei.setCreatedate(java.sql.Date.valueOf(date));
        }
        List<Koubei> koubeis = koubeiService.selectByBrand(koubei);

        DecimalFormat df   = new DecimalFormat("######0.00");
        result.setExterior(0.0);
        result.setInterior(0.0);
        result.setCarspace(0.0);
        result.setOptionsortrim(0.0);
        result.setPower(0.0);
        result.setManeuverability(0.0);
        result.setFuelconsumption(0.0);
        result.setComfort(0.0);

        result.setEnglishbrand(koubeis.get(0).getEnglishbrand());

        for (int i=0;i<koubeis.size();i++){
            result.setExterior(result.getExterior()+koubeis.get(i).getExterior());
            result.setInterior(result.getInterior()+koubeis.get(i).getInterior());
            result.setCarspace(result.getCarspace()+koubeis.get(i).getCarspace());
            result.setOptionsortrim(result.getOptionsortrim()+koubeis.get(i).getOptionsortrim());
            result.setPower(result.getPower()+koubeis.get(i).getPower());
            result.setManeuverability(result.getManeuverability()+koubeis.get(i).getManeuverability());
            result.setFuelconsumption(result.getFuelconsumption()+koubeis.get(i).getFuelconsumption());
            result.setComfort(result.getComfort()+koubeis.get(i).getComfort());
        }

        result.setId(1);
        result.setExterior(Double.valueOf(df.format(result.getExterior()/koubeis.size())));
        result.setInterior(Double.valueOf(df.format(result.getInterior()/koubeis.size())));
        result.setCarspace(Double.valueOf(df.format(result.getCarspace()/koubeis.size())));
        result.setOptionsortrim(Double.valueOf(df.format(result.getOptionsortrim()/koubeis.size())));
        result.setPower(Double.valueOf(df.format(result.getPower()/koubeis.size())));
        result.setManeuverability(Double.valueOf(df.format(result.getManeuverability()/koubeis.size())));
        result.setFuelconsumption(Double.valueOf(df.format(result.getFuelconsumption()/koubeis.size())));
        result.setComfort(Double.valueOf(df.format(result.getComfort()/koubeis.size())));


//       选出strength和weaknesses
        result.setStrengths("Exterior");
        Double strengthvalue = result.getExterior();
        if(strengthvalue<result.getInterior()){
            result.setStrengths("Interior");
            strengthvalue = result.getInterior();
        }
        if(strengthvalue<result.getCarspace()){
            result.setStrengths("Carspace");
            strengthvalue = result.getCarspace();
        }
        if(strengthvalue<result.getOptionsortrim()){
            result.setStrengths("Options/trim");
            strengthvalue = result.getOptionsortrim();
        }
        if(strengthvalue<result.getPower()){
            result.setStrengths("Power");
            strengthvalue = result.getPower();
        }
        if(strengthvalue<result.getManeuverability()){
            result.setStrengths("Maneuverability");
            strengthvalue = result.getManeuverability();
        }
        if(strengthvalue<result.getFuelconsumption()){
            result.setStrengths("Fuel consumption");
            strengthvalue = result.getFuelconsumption();
        }
        if(strengthvalue<result.getComfort()){
            result.setStrengths("Comfort");
            strengthvalue = result.getComfort();
        }

//        选出弱项
        result.setWeaknesses("Exterior");
        Double weaknesses = result.getExterior();
        if(weaknesses>result.getInterior()){
            result.setWeaknesses("Interior");
            weaknesses = result.getInterior();
        }
        if(weaknesses>result.getCarspace()){
            result.setWeaknesses("Carspace");
            weaknesses = result.getCarspace();
        }
        if(weaknesses>result.getOptionsortrim()){
            result.setWeaknesses("Options/trim");
            weaknesses = result.getOptionsortrim();
        }
        if(weaknesses>result.getPower()){
            result.setWeaknesses("Power");
            weaknesses = result.getPower();
        }
        if(weaknesses>result.getManeuverability()){
            result.setWeaknesses("Maneuverability");
            weaknesses = result.getManeuverability();
        }
        if(weaknesses>result.getFuelconsumption()){
            result.setWeaknesses("Fuel consumption");
            weaknesses = result.getFuelconsumption();
        }
        if(weaknesses>result.getComfort()){
            result.setWeaknesses("Comfort");
            weaknesses = result.getComfort();
        }

        result.setDatanum(koubeis.size());
        return result;

    }

    @RequestMapping("/querydate")
    public List<String> querydate() throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-30");//格式化为年月

        String  minDate = "2020-09-30";
        String maxDate = sdf.format(new Date());

        ArrayList<String> result = new ArrayList<String>();

        Calendar min = Calendar.getInstance();
        Calendar max = Calendar.getInstance();

        min.setTime(sdf.parse(minDate));
        min.set(min.get(Calendar.YEAR), min.get(Calendar.MONTH), 1);

        max.setTime(sdf.parse(maxDate));
        max.set(max.get(Calendar.YEAR), max.get(Calendar.MONTH), 2);

        Calendar curr = min;
        while (curr.before(max)) {
            result.add(sdf.format(curr.getTime()));
            curr.add(Calendar.MONTH, 1);
        }

        return result;
    }
}