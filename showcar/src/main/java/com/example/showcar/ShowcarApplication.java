package com.example.showcar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShowcarApplication {

    public static void main(String[] args) {
        SpringApplication.run(ShowcarApplication.class, args);
    }


}
