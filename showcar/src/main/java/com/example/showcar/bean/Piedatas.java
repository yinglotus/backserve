package com.example.showcar.bean;

import lombok.Data;

import java.io.Serializable;

/**
 * (Piedatas)实体类
 *
 * @author makejava
 * @since 2020-11-13 11:02:14
 */
@Data
public class Piedatas implements Serializable {
    private static final long serialVersionUID = 877405451708540976L;
    
    private Integer id;
    
    private String brand;
    
    private String category;
    
    private String details;
    
    private Double categoryrate;
    
    private Double detailsrate;

    private String commenttype;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public Double getCategoryrate() {
        return categoryrate;
    }

    public void setCategoryrate(Double categoryrate) {
        this.categoryrate = categoryrate;
    }

    public Double getDetailsrate() {
        return detailsrate;
    }

    public void setDetailsrate(Double detailsrate) {
        this.detailsrate = detailsrate;
    }

}