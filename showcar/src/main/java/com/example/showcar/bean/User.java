package com.example.showcar.bean;

import lombok.Data;

import java.io.Serializable;

/**
 * (User)实体类
 *
 * @author makejava
 * @since 2020-08-02 15:30:02
 */
@Data
public class User implements Serializable {
    private static final long serialVersionUID = -30337457420095330L;
    
    private Integer id;
    
    private String username;
    
    private String password;


}