package com.example.showcar.bean;

import lombok.Data;

import java.io.Serializable;
import java.sql.Date;

/**
 * (Koubei)实体类
 *
 * @author makejava
 * @since 2020-08-21 20:01:07
 */
@Data
public class Koubei implements Serializable {
    private static final long serialVersionUID = -30052283646989093L;
    
    private Integer id;
    
    private String brand;

    private String englishbrand;

    private String carseries;
    
    private String model;
    
    private Double exterior;
    
    private Double interior;

    private Double carspace;
    
    private Double optionsortrim;
    
    private Double power;
    
    private Double maneuverability;
    
    private Double fuelconsumption;
    
    private Double comfort;

    private Date createdate;

    private String strengths;

    private String weaknesses;

    private Integer datanum;


}