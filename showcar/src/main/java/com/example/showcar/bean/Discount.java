package com.example.showcar.bean;

import lombok.Data;

import java.io.Serializable;

/**
 * (Discount)实体类
 *
 * @author makejava
 * @since 2020-10-31 20:23:56
 */
@Data
public class Discount implements Serializable {
    private static final long serialVersionUID = -96823409028678357L;
    
    private Integer id;
    
    private String brand;

    private String englishbrand;
    
    private String carseries;
    
    private String model;
    
    private String luoche;
    
    private String zhidao;
    
    private String buytime;
    
    private String buyyear;
    
    private String buymonth;
    
    private String buyplace;
    
    private String pricedifference;
    
    private String discount;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getCarseries() {
        return carseries;
    }

    public void setCarseries(String carseries) {
        this.carseries = carseries;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getLuoche() {
        return luoche;
    }

    public void setLuoche(String luoche) {
        this.luoche = luoche;
    }

    public String getZhidao() {
        return zhidao;
    }

    public void setZhidao(String zhidao) {
        this.zhidao = zhidao;
    }

    public String getBuytime() {
        return buytime;
    }

    public void setBuytime(String buytime) {
        this.buytime = buytime;
    }

    public String getBuyyear() {
        return buyyear;
    }

    public void setBuyyear(String buyyear) {
        this.buyyear = buyyear;
    }

    public String getBuymonth() {
        return buymonth;
    }

    public void setBuymonth(String buymonth) {
        this.buymonth = buymonth;
    }

    public String getBuyplace() {
        return buyplace;
    }

    public void setBuyplace(String buyplace) {
        this.buyplace = buyplace;
    }

    public String getPricedifference() {
        return pricedifference;
    }

    public void setPricedifference(String pricedifference) {
        this.pricedifference = pricedifference;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

}