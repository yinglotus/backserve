package com.example.showcar.bean;

import lombok.Data;

@Data
public class RetunDataOfByBrand {

    private Integer id;

    private String brand;

    private String englishBrand;

    private String carseries;

    private String buyyear;

    private String buymonth;

    private String englishbuymonth;

    private String discount;
}
