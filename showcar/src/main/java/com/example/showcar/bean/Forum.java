package com.example.showcar.bean;

import lombok.Data;

import java.io.Serializable;

/**
 * (Forum)实体类
 *
 * @author makejava
 * @since 2020-08-21 20:01:05
 */
@Data
public class Forum implements Serializable {
    private static final long serialVersionUID = -78886435271069642L;
    
    private Integer id;
    
    private String brand;
    
    private String model;
    
    private Double exterior;
    
    private Double interior;
    
    private Double carspace;
    
    private Double optionsortrim;
    
    private Double power;
    
    private Double maneuverability;
    
    private Double fuelconsumption;
    
    private Double comfort;
    
    private String forumname;
    
    private String author;
    
    private String content;


}