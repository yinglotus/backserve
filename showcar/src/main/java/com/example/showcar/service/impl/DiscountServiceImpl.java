package com.example.showcar.service.impl;

import com.example.showcar.bean.Discount;
import com.example.showcar.dao.DiscountDao;
import com.example.showcar.service.DiscountService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * (Discount)表服务实现类
 *
 * @author makejava
 * @since 2020-10-31 20:23:58
 */
@Service("discountService")
public class DiscountServiceImpl implements DiscountService {

    @Resource
    private DiscountDao discountDao;

    /**
            * 根绝品牌、年、月查询
     */
    @Override
    public List<Discount> selectByBrandYearMonth(Discount discount){
        return this.discountDao.selectByBrandYearMonth(discount);
    }

    /**
     * 根据车系、年、月查询
     */
    @Override
    public List<Discount> selectByCarseries(Discount discount){
        return this.discountDao.selectByCarseries(discount);
    }

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public Discount queryById(Integer id) {
        return this.discountDao.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    @Override
    public List<Discount> queryAllByLimit(int offset, int limit) {
        return this.discountDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param discount 实例对象
     * @return 实例对象
     */
    @Override
    public Discount insert(Discount discount) {
        this.discountDao.insert(discount);
        return discount;
    }

    /**
     * 修改数据
     *
     * @param discount 实例对象
     * @return 实例对象
     */
    @Override
    public Discount update(Discount discount) {
        this.discountDao.update(discount);
        return this.queryById(discount.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Integer id) {
        return this.discountDao.deleteById(id) > 0;
    }
}