package com.example.showcar.service.impl;

import com.example.showcar.bean.Piedatas;
import com.example.showcar.dao.PiedatasDao;
import com.example.showcar.service.PiedatasService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * (Piedatas)表服务实现类
 *
 * @author makejava
 * @since 2020-11-13 11:02:17
 */
@Service("piedatasService")
public class PiedatasServiceImpl implements PiedatasService {
    @Resource
    private PiedatasDao piedatasDao;

    /**
     *根据brand查询category
     */
    @Override
    public List<Piedatas> queryAllcategoryByBrand(Piedatas piedatas){
        return this.piedatasDao.queryAllcategoryByBrand(piedatas);
    }


    /**
     * 通过实体作为筛选条件查询
     *
     * @param piedatas 实例对象
     * @return 对象列表
     */
    @Override
    public List<Piedatas> queryAll(Piedatas piedatas){
        return this.piedatasDao.queryAll(piedatas);
    }

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public Piedatas queryById(Integer id) {
        return this.piedatasDao.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    @Override
    public List<Piedatas> queryAllByLimit(int offset, int limit) {
        return this.piedatasDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param piedatas 实例对象
     * @return 实例对象
     */
    @Override
    public Piedatas insert(Piedatas piedatas) {
        this.piedatasDao.insert(piedatas);
        return piedatas;
    }

    /**
     * 修改数据
     *
     * @param piedatas 实例对象
     * @return 实例对象
     */
    @Override
    public Piedatas update(Piedatas piedatas) {
        this.piedatasDao.update(piedatas);
        return this.queryById(piedatas.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Integer id) {
        return this.piedatasDao.deleteById(id) > 0;
    }
}