package com.example.showcar.service;

import com.example.showcar.bean.Piedatas;
import java.util.List;

/**
 * (Piedatas)表服务接口
 *
 * @author makejava
 * @since 2020-11-13 11:02:16
 */
public interface PiedatasService {

    /**
     *根据brand查询category
     */
    List<Piedatas> queryAllcategoryByBrand(Piedatas piedatas);

    /**
     * 通过实体作为筛选条件查询
     *
     * @param piedatas 实例对象
     * @return 对象列表
     */
    List<Piedatas> queryAll(Piedatas piedatas);

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    Piedatas queryById(Integer id);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<Piedatas> queryAllByLimit(int offset, int limit);

    /**
     * 新增数据
     *
     * @param piedatas 实例对象
     * @return 实例对象
     */
    Piedatas insert(Piedatas piedatas);

    /**
     * 修改数据
     *
     * @param piedatas 实例对象
     * @return 实例对象
     */
    Piedatas update(Piedatas piedatas);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(Integer id);

}