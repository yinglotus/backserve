package com.example.showcar.service;

import com.example.showcar.bean.Discount;
import java.util.List;

/**
 * (Discount)表服务接口
 *
 * @author makejava
 * @since 2020-10-31 20:23:58
 */
public interface DiscountService {
    /**
     * 根绝品牌、年、月查询
     */
    List<Discount> selectByBrandYearMonth(Discount discount);

    /**
     * 根据车系、年、月、查询
     */
    List<Discount> selectByCarseries(Discount discount);

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    Discount queryById(Integer id);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<Discount> queryAllByLimit(int offset, int limit);

    /**
     * 新增数据
     *
     * @param discount 实例对象
     * @return 实例对象
     */
    Discount insert(Discount discount);

    /**
     * 修改数据
     *
     * @param discount 实例对象
     * @return 实例对象
     */
    Discount update(Discount discount);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(Integer id);

}