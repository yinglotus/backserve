package com.example.showcar.service;

import com.example.showcar.bean.Forum;
import java.util.List;

/**
 * (Forum)表服务接口
 *
 * @author makejava
 * @since 2020-08-21 18:30:27
 */
public interface ForumService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    Forum queryById(Integer id);

    /**
     * 查询所有
     */
    List<Forum> queryAll(Forum forum);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<Forum> queryAllByLimit(int offset, int limit);

    /**
     * 新增数据
     *
     * @param forum 实例对象
     * @return 实例对象
     */
    Forum insert(Forum forum);

    /**
     * 修改数据
     *
     * @param forum 实例对象
     * @return 实例对象
     */
    Forum update(Forum forum);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(Integer id);

}