package com.example.showcar.service;

import com.example.showcar.bean.Koubei;
import java.util.List;

/**
 * (Koubei)表服务接口
 *
 * @author makejava
 * @since 2020-08-08 16:01:43
 */
public interface KoubeiService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    Koubei queryById(Integer id);

    /**
     * 查询所有
     */
    List<Koubei> queryAll(Koubei koubei);

    /**
     * 通过日期查询所有的数据
     */
    List<Koubei> queryAllNumByDate(Koubei koubei);


    /**
     * queryBycarseries
     */
    List<Koubei> queryBycarseries(Koubei koubei);

    /**
     * 选择日期
     */
    List<Koubei> querydate();

    /**
     * selectByBrand
     */
    List<Koubei> selectByBrand(Koubei koubei);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<Koubei> queryAllByLimit(int offset, int limit);

    /**
     * 新增数据
     *
     * @param koubei 实例对象
     * @return 实例对象
     */
    Koubei insert(Koubei koubei);

    /**
     * 修改数据
     *
     * @param koubei 实例对象
     * @return 实例对象
     */
    Koubei update(Koubei koubei);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(Integer id);

}