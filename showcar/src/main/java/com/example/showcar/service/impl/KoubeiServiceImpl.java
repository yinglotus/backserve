package com.example.showcar.service.impl;

import com.example.showcar.bean.Koubei;
import com.example.showcar.dao.KoubeiDao;
import com.example.showcar.service.KoubeiService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * (Koubei)表服务实现类
 *
 * @author makejava
 * @since 2020-08-08 16:01:44
 */
@Service("koubeiService")
public class KoubeiServiceImpl implements KoubeiService {
    @Resource
    private KoubeiDao koubeiDao;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public Koubei queryById(Integer id) {
        return this.koubeiDao.queryById(id);
    }

    /**
     * 查询所有
     */
    @Override
    public List<Koubei> queryAll(Koubei koubei){
        return this.koubeiDao.queryAll(koubei);
    }

    /**
     * 通过日期查询所有数据
     * @param koubei
     * @return
     */
    @Override
    public List<Koubei> queryAllNumByDate(Koubei koubei){
        return this.koubeiDao.queryAllNumByDate(koubei);
    }


    /**
     * queryBycarseries
     */
    @Override
    public List<Koubei> queryBycarseries(Koubei koubei){
        return this.koubeiDao.queryBycarseries(koubei);
    }

    /**
     * 选择口碑
     */
    @Override
    public List<Koubei> querydate(){
        return this.koubeiDao.querydate();
    }

    /**
     * selectByBrand
     */
    @Override
    public List<Koubei> selectByBrand(Koubei koubei){
        return this.koubeiDao.selectByBrand(koubei);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    @Override
    public List<Koubei> queryAllByLimit(int offset, int limit) {
        return this.koubeiDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param koubei 实例对象
     * @return 实例对象
     */
    @Override
    public Koubei insert(Koubei koubei) {
        this.koubeiDao.insert(koubei);
        return koubei;
    }

    /**
     * 修改数据
     *
     * @param koubei 实例对象
     * @return 实例对象
     */
    @Override
    public Koubei update(Koubei koubei) {
        this.koubeiDao.update(koubei);
        return this.queryById(koubei.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Integer id) {
        return this.koubeiDao.deleteById(id) > 0;
    }
}