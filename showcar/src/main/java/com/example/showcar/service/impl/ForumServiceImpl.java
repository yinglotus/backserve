package com.example.showcar.service.impl;

import com.example.showcar.bean.Forum;
import com.example.showcar.dao.ForumDao;
import com.example.showcar.service.ForumService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * (Forum)表服务实现类
 *
 * @author makejava
 * @since 2020-08-21 18:30:27
 */
@Service("forumService")
public class ForumServiceImpl implements ForumService {
    @Resource
    private ForumDao forumDao;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public Forum queryById(Integer id) {
        return this.forumDao.queryById(id);
    }

    /**
     * 查询所有
     */
    @Override
    public List<Forum> queryAll(Forum forum){
        return this.forumDao.queryAll(forum);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    @Override
    public List<Forum> queryAllByLimit(int offset, int limit) {
        return this.forumDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param forum 实例对象
     * @return 实例对象
     */
    @Override
    public Forum insert(Forum forum) {
        this.forumDao.insert(forum);
        return forum;
    }

    /**
     * 修改数据
     *
     * @param forum 实例对象
     * @return 实例对象
     */
    @Override
    public Forum update(Forum forum) {
        this.forumDao.update(forum);
        return this.queryById(forum.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Integer id) {
        return this.forumDao.deleteById(id) > 0;
    }
}