package com.example.showcar.util;

import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.HttpClientUtils;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
@RestController
@RequestMapping("/pachong")
public class HttpClientDownPage {

    @RequestMapping("/test")
    public String test() {
        //1. 得到网页源码
        //1.1 java.net.URL方式
        //1.2 jsoup 或者用这种方法
        //设置header目的是为了伪装成游览器访问
        Connection conn = Jsoup.connect("http://m.sinovision.net/newpneumonia.php")
                .maxBodySize(0)
                .header("User-Agent","Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36")
                .header("Connection","keep-alive")
                .timeout(30000);

        //执行
        Connection.Response res = null;
        try {
            res = conn.execute();

            Document doc = Jsoup.parse(res.body());
            Elements es = doc.getElementsByClass("todaydata");
            Element e6 = es.get(6);

            Elements blocks = e6.getElementsByClass("main-block");
        } catch (IOException e) {
            e.printStackTrace();
        }
        //2. 解析出数据 组装成List<Province>

        return "123";
    }
}
