package com.example.showcar.dao;

import com.example.showcar.bean.Koubei;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * (Koubei)表数据库访问层
 *
 * @author makejava
 * @since 2020-08-08 16:01:42
 */
@Mapper
public interface KoubeiDao {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    Koubei queryById(Integer id);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<Koubei> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * queryBycarseries
     */
    List<Koubei> queryBycarseries(Koubei koubei);

    /**
     * selectByBrand
     */
    List<Koubei> selectByBrand(Koubei koubei);

    /**
     * 通过日期查询所有的数据
     */
    List<Koubei> queryAllNumByDate(Koubei koubei);

    /**
     * 选择日期
     */
    List<Koubei> querydate();

    /**
     * 通过实体作为筛选条件查询
     *
     * @param koubei 实例对象
     * @return 对象列表
     */
    List<Koubei> queryAll(Koubei koubei);

    /**
     * 新增数据
     *
     * @param koubei 实例对象
     * @return 影响行数
     */
    int insert(Koubei koubei);

    /**
     * 修改数据
     *
     * @param koubei 实例对象
     * @return 影响行数
     */
    int update(Koubei koubei);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(Integer id);

}