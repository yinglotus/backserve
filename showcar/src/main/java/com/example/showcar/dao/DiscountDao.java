package com.example.showcar.dao;

import com.example.showcar.bean.Discount;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * (Discount)表数据库访问层
 *
 * @author makejava
 * @since 2020-10-31 20:23:58
 */
@Mapper
public interface DiscountDao {

    /**
     * 根据品牌、年、月查询
     */
    List<Discount> selectByBrandYearMonth(Discount discount);

    /**
     * 根据车系、年、月查询
     */
    List<Discount> selectByCarseries(Discount discount);

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    Discount queryById(Integer id);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<Discount> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param discount 实例对象
     * @return 对象列表
     */
    List<Discount> queryAll(Discount discount);

    /**
     * 新增数据
     *
     * @param discount 实例对象
     * @return 影响行数
     */
    int insert(Discount discount);

    /**
     * 修改数据
     *
     * @param discount 实例对象
     * @return 影响行数
     */
    int update(Discount discount);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(Integer id);

}